#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <random>
#include <iterator>

std::random_device r;
std::default_random_engine en(r());
std::uniform_int_distribution<int> uniform_dist(1,1000);

void print_vector(std::vector<int> &vec){
    std::ostream_iterator<int> out_itr(std::cout,", ");
    std::copy(vec.begin(), vec.end(), out_itr);

}
struct prime_number_finder{
    std::vector<int>numbers;
    int dcounter{0};
    void find(){
        for(int i = 0; i<1000; i++) {

            for (int j = i; j > 0; j--){
                if(std::modulus<int>()(i,j)==0) {
                    dcounter++;
                }


            }
            if(dcounter==2) {
                numbers.push_back(i);
            }
            dcounter=0;
        }
    }
};

struct create_map{
    std::map<int,std::vector<int>> myMap;
    std::vector<int> generated;
    int random_numbers_generator() {

        return uniform_dist(en);
    }
    void create(std::vector<int> &vec) {
        for (int i = 0; i < 100; i++) {
            generated.push_back(random_numbers_generator());
        }
        for (auto add_empty_vectors_to_key:vec) {
            myMap[add_empty_vectors_to_key] = std::vector<int>();
        }
        for (auto find_prime_dividers_generated:generated) {
            for (auto push_values_to_empty_vectors_if_prime_mumber_is_divider:vec) {
                if (std::modulus<int>()(find_prime_dividers_generated, push_values_to_empty_vectors_if_prime_mumber_is_divider) == 0) {
                    myMap[push_values_to_empty_vectors_if_prime_mumber_is_divider].push_back(find_prime_dividers_generated);
                }
            }
        }
    }
};

int main() {

    prime_number_finder finder0;
    finder0.find();
    print_vector(finder0.numbers);
    create_map create_map0;
    create_map0.create(finder0.numbers);

    for(auto prt:create_map0.myMap){
        std::cout<<"M: "<<prt.first << ": ";
        std::sort(prt.second.begin(), prt.second.end());

        print_vector(prt.second);

        std::cout<<std::endl;
    }



    return 0;
}